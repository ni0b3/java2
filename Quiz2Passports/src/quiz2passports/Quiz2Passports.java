package quiz2passports;

import java.util.*;

class InvalidValueException extends Exception {

    InvalidValueException(String message) {
        super(message);
    }
}

class Passport {

    public Passport(String number, String firstName, String lastName, String address, String city, double heightCm, double weightKg, int yob) throws Exception {
        setNumber(number);
        setFirstName(firstName);
        setLastName(lastName);
        setAddress(address);
        setCity(city);
        setHeightCm(heightCm);
        setWeightKg(weightKg);
        setYob(yob);
    }

    public void print() {
        System.out.printf("Passport #%s, First name: %s, Last name: %s, Address: %s, City: %s, Height: %.2f, Weight: %.2f, DOB: %d%n", number, firstName, lastName, address, city, heightCm, weightKg, yob);
    }

    private String number; // passport number AB123456 format exactly
    private String firstName, lastName, address, city; // at least 2 letters each
    private double heightCm, weightKg; // height from 0-300, weight 0-300
    private int yob; // year of birth - between 1900-2050

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) throws Exception {
        if (number.length() < 0 || number.length() > 8) {
            throw new Exception("Invalid entry");
        }
        this.number = number;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) throws Exception {
        if (firstName.length() < 2) {
            throw new Exception("First name must be min 2 Characters long");
        }
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) throws Exception {
        if (lastName.length() < 2) {
            throw new Exception("Last name must be a min of 2 Characters");
        }
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) throws Exception {
        if (address.length() < 2) {
            throw new Exception("Address must be a min of 2 Characters");
        }
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) throws Exception {
        if (city.length() < 2) {
            throw new Exception("City name must be a min of 2 Characters");
        }
        this.city = city;
    }

    public double getHeightCm() {
        return heightCm;
    }

    public void setHeightCm(double heightCm) throws Exception {
        if (heightCm < 0 || heightCm > 300) {
            throw new Exception("Height must be from 0-300cm");
        }
        this.heightCm = heightCm;
    }

    public double getWeightKg() {
        return weightKg;
    }

    public void setWeightKg(double weightKg) throws Exception {
        if (weightKg < 0 || weightKg > 300) {
            throw new Exception("Weight must be 0-300Kg");
        }
        this.weightKg = weightKg;
    }

    public int getYob() {
        return yob;
    }

    public void setYob(int yob) throws Exception {

        if (yob < 1900 || yob > 2050) {
            throw new Exception("Enter a date of birth Between 1900-2050");
        }

        this.yob = yob;
    }

}

public class Quiz2Passports {

    static ArrayList<Passport> passList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);

    public static int mainMenu() {
        int choice;
        System.out.println();
        System.out.println("1. Display all passports data ");
        System.out.println("2. Display all passports for people in the same city");
        System.out.println("3. Find the tallest person and display their info");
        System.out.println("4. Find the lightest person and display their info");
        System.out.println("5. Display all people younger than certain age ");
        System.out.println("6. Add person/passport to list");
        System.out.println("0. Exit");
        choice = input.nextInt();
        input.nextLine();

        switch (choice) {
            case 0:
                System.out.println("Exiting. Good bye!");
                System.exit(1);
            case 1:
                displayPassportData();
                break;
            case 2:
                displayPassportCity();
                break;
            case 3:
                findTallest();
                break;
            case 4:
                findLightest();
                break;
            case 5:
                displayYoungerThan();
                break;
            case 6:
                addPerson();
                break;
            default:
                if (choice < 0 || choice > 6) {
                    System.out.println("Invalid choice, try again.");
                }
        }
        return choice;
    }

    private static void displayPassportData() {
        for (int i = 0; i < passList.size(); i++) {//get the size of the array
            System.out.print(i + 1 + ". ");
            passList.get(i).print();
        }
    }

    private static void displayPassportCity() {
        try {

            System.out.println("**************Finding Clients in City**************");
            System.out.println("Which City");
            String city = input.nextLine();
            
            for (int i = 0; i < passList.size(); i++) {
               
                if (city == passList.get(i).getCity()) {
                    System.out.printf("Client From City %s: %n", city);
                    passList.get(i).print();
                }
            }
            

        } catch (Exception ex) {
            System.out.println("Error no City Available " + ex.getMessage());
        }
    }

    private static void findTallest() {
        try {

            System.out.println("**************Finding Tallest Client**************");

            for (int i = 0; i < passList.size(); i++) {
                for (int j = 0; j < passList.size(); j++) {
                    if (passList.get(j).getHeightCm() < passList.get(i).getHeightCm()) {
                        //int tallest=(int) passList.get(j).getHeightCm();
                        System.out.print("The Tallest Person Is: ");
                        passList.get(i).print();

                    }
                }
            }
            input.nextLine(); // consume the left over newline character

        } catch (Exception ex) {
            System.out.println("Error it looks like we can fit more people because everyone is a Little Person " + ex.getMessage());
        }
    }

    private static void findLightest() {
        try {

            System.out.println("**************Finding Lightest Client**************");

            for (int i = 0; i < passList.size(); i++) {
                for (int j = 0; j < passList.size(); j++) {
                    if (passList.get(i).getWeightKg() < passList.get(j).getWeightKg()) {
                        //int lightest=(int)passList.get(i).getWeightKg();
                        System.out.print("The Lightest Person Is: ");
                        passList.get(i).print();

                    }
                }
            }
            input.nextLine(); // consume the left over newline character

        } catch (Exception ex) {
            System.out.println("Error Everyone is skinny so it doesnt matter " + ex.getMessage());
        }
    }

    private static void displayYoungerThan() {

        try {

            System.out.println("**************Clients Ages Below**************");

            int year = 2017;
            System.out.print("What is the age of the oldest person? ");
            int age = input.nextInt();
            int ageYear = year - age;
            for (int i = 0; i < passList.size(); i++) {
                if (ageYear < passList.get(i).getYob()) {
                    System.out.print(i + 1 + ". ");
                    passList.get(i).print();
                }
            }
            input.nextLine(); // consume the left over newline character

        } catch (Exception ex) {
            System.out.println("Error creating list " + ex.getMessage());
        }
    }

    private static void addPerson() {
        try {
            System.out.println("**************Adding a Client**************");
            System.out.print("Enter Passport # : ");
            String number = input.nextLine();
            System.out.print("Enter First Name: ");
            String firstName = input.nextLine();
            System.out.print("Enter Last Name: ");
            String lastName = input.nextLine();
            System.out.print("Enter Address: ");
            String address = input.nextLine();
            System.out.print("Enter City: ");
            String city = input.nextLine();
            System.out.print("Enter Height: ");
            double heightCm = input.nextDouble();
            System.out.print("Enter Weight: ");
            double weightKg = input.nextDouble();
            System.out.print("Enter Year of Birth: ");
            int yob = input.nextInt();
            input.nextLine(); // consume the left over newline character
            Passport person = new Passport(number, firstName, lastName, address, city, heightCm, weightKg, yob);
            passList.add(person);
            System.out.println("*************************************");
            System.out.println("You've created the following Cleint:");
            person.print();
            System.out.println("*************************************");
            System.out.println();
        } catch (Exception ex) {
            System.out.println("Error creating Client: " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        while (true) {
            mainMenu();
        }
    }

}
