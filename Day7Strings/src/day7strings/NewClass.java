package day7strings;

class Data{
    public int v;
}

public class NewClass {
    
    static Data method(int i, Data d){
        d.v =i+3;
        System.out.println("d="+d.v);
        d=new Data();
        System.out.println("D="+d.v);
        i +=2;
        d.v =i;
        System.out.println("d="+d.v);
        return d;
    }
    public static void main(String args[]) {
        Data x = new Data();
        x.v=7;
        Data y=method(1,x);
        System.out.println("X="+x.v+", y="+y.v);
    }
}
