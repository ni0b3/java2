/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day7strings;
import java.util.*;
/**
 *
 * @author ipd13
 */
public class Day7Strings {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Scanner input = new Scanner(System.in);
       System.out.print("Enter your name:");
       String name=input.nextLine();
       if(name.equals(new String ("Santa")))
       {
           System.out.println("Wow! Really? Santa!");
       }
       else
       {
           System.out.println("Hi nice to meet you "+ name);
       }
    } 
    
}
