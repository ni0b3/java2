// 1.

document.getElementsByTagName("p")[0].innerHTML = "Dwayne Williams";

// 2.

let today = new Date();

document.getElementsByClassName("date-tag")[0].lastElementChild.innerHTML = today;

// 3.

let gradesCol = document.getElementsByClassName("grade");
let grades = [];
let sum = 0;

function calculateAvg(){
    
    for (var i = 0; i < gradesCol.length; i++) {
        sum += +gradesCol[i].textContent;
        grades[i] = +gradesCol[i].textContent;
    }
    
    return sum/gradesCol.length;
}
    
document.querySelectorAll(".average")[1].innerHTML = calculateAvg().toFixed(2);

// 4.

function updateImg(){
    let img = document.getElementsByTagName("img")[0];
    let value = document.querySelectorAll(".average")[1].innerHTML;
    
    if (value>80){
        
        img.src = "images/check.jpg";
        
    }  else if (value<80 && value>0){
        
        img.src = "images/Xmark.png";
    } else {
        
        img.src = "images/question.png";
    }
    
    
    
}

updateImg();

// 5.

function updateSummary(){
    
    
    //a, b. Highest and lowest grades

    let max = grades[0];
    let min = grades[0];
    
    for (var i = 0; i < grades.length; i++) {
        if (grades[i]>max){
            
            max = grades[i];
            
        } 
        if (grades[i]<min){
            
            min = grades[i];
            
        }
    }
    
    // c, d. number of 100% and <70%
    let cnt100 = 0;
    let cnt70 = 0;
    for (let l of grades){
        
        if (l === 100) {
            cnt100++;
        } else if (l < 70) {
            
            cnt70++;
            
        }
    }
    // e. number of excellent students
    let cntExcellent = 0;
    for (let l of document.querySelectorAll(".notes")){
        
        if (l.textContent.toLowerCase().indexOf("excellent") !== -1)
            cntExcellent++;
        
    }
    
    
    document.querySelector(".summary").lastElementChild.innerHTML = 
            
    "<b> The highest grade is: </b>" + max + "<br>" +
    "<b> The lowest grade is: </b>" + min + "<br>" +
    "<b> The number of students with 100%: </b>" + cnt100 + "<br>" +
    "<b> The number of students who failed (<70%): </b>" + cnt70 + "<br>"+
    "<b> The number of students with \"Excellent\" mark: </b>" + cntExcellent;
            
}
updateSummary();