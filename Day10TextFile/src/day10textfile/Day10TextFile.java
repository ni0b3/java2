/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10textfile;

import java.io.*;
import java.util.*;

public class Day10TextFile {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter a line of text: ");
        String line = input.nextLine();

        try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter("output.txt")))) {
            pw.println(line);
            pw.println(line);
            pw.println(line);
        } catch (IOException ex) {
            System.out.println("Writing to the file has generated a error" + ex.getMessage());
        }

    }

    {/// reading from file
        try (Scanner fileInput = new Scanner(new File("output.txt"))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                System.out.println("Line retrieved was: " + line);
            }
        } catch (IOException ex) {
            System.out.println("File reading error" + ex.getMessage());
        }
    }

}
