/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Array;

import java.util.*;

/**
 *
 * @author ipd13
 */
public class GradeToGPA {

    // to iniate the variable for user input
    public static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {

        double grade = 0;
        int count = 0;
        double sum =0;
        String gradeStr;
        
        gradeStr = input.nextLine();
        while (!"0".equals(gradeStr))
        {
        System.out.println("Enter the Studen grade:");
        gradeStr = input.nextLine().toUpperCase();
            switch (gradeStr) {
                case "A":
                    grade = 4.0;
                    break;
                case "B":
                    grade = 3.7;
                    break;
                case "C":
                    grade = 2.5;
                    break;
                case "D":
                    grade = 2.0;
                    break;
                case "-A":
                    grade = 3.8;
                    break;
                case "-B":
                    grade = 5.5;
                    break;
                case "-C":
                    grade = 5.0;
                    break;
                case "-D":
                    grade = 6.0;
                    break;
                case "F":
                    grade = 7.0;
                    break;
                default:
                    System.out.println("This person did not take the course or there grade is just not on the chart");
                    break;
            }
            System.out.printf("The Studen GPA is %.1f", grade);
            count++;
            sum += grade;
        }
       
        System.out.printf("There is a total number of %d grades entered %n", count);
        System.out.printf("The Sum of the grades entered is: %.1f", sum/count);
    }

}
