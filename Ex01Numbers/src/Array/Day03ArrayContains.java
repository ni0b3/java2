/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Array;

//import java.util.*;
import java.util.Arrays;

/**
 *
 * @author ipd13
 */
public class Day03ArrayContains {

    public static void printDups(int[] a1, int[] a2) {
        for (int i = 0; i < a1.length; i++) {
            for (int j = 0; j < a2.length; j++) {
                if (a1[i] == a2[j]) {
                    System.out.println(a1[i]);
                    break;
                }
            }
        }
    }

    public static int[] removeDups(int[] a1, int[] a2) {
        //declare a counter to count the amount of non duplicates
        int nonDupCount = a1.length;
        int[] a3 = new int[nonDupCount];
        for (int i = 0; i < a1.length; i++) {
            for (int j = 0; j < a2.length; j++) {
                if (a1[i] == a2[j]) {
                    nonDupCount--;
                    break;
                }
                // assign a new list based on the number of non duplicates

            }
        }
        //Print the number of duplicates
        System.out.println("Non dup count:" + nonDupCount);

        //verify that the there are no duplicates and print the non duplicates
        for (int i = 0; i < a1.length; i++) {
            for (int j = 0; j < a2.length; j++) {
                if (a1[i] != a2[j]) {
                    System.out.println(a1[i]);
                    break;
                }
                a3[i] = a1[i];
                System.out.println(Arrays.toString(a3));
            }
        }
        return null;
    }

    public static int[] concatenate(int[] a1, int[] a2) {
        int result[] = new int[a1.length + a2.length];
        for (int i = 0; i < a1.length; i++) {
            result[i] = a1[i];
        }
        for (int j = 0; j < a2.length; j++) {
            result[j+a1.length] = a2[j];
        }
        return result;
    }

    public static void main(String[] args) {
        int[] a1 = {1, 3, 7, 8, 2, 7, 9, 11};
        int[] a2 = {3, 8, 7, 5, 13, 5, 12, 7};
        printDups(a1, a2);
        removeDups(a1, a2);
    }
}
