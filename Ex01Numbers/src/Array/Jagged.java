/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Array;

/**
 *
 * @author ipd13
 */
public class Jagged {
    
    public static void main(String[] args) {
        int [][] twoJagged= {{1,2,3},{4,5},{6,7,8,9,10}};
        
        for(int row=0;row<twoJagged.length; row++){
            for(int col=0; col<twoJagged[row].length; col++){
                int val=twoJagged[row][col];
                System.out.printf("%s%d",(col==0?"": ", "), val);
            }
        }
    }
    
}
