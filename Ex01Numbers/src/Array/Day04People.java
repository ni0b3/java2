/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Array;
import java.util.*;
/**
 *
 * @author ipd13
 */
public class Day04People {
    
    public static void main(String[] args){
        String[] nameArray= new String[4];
        int[] ageArray = new int[4];
        Scanner input = new Scanner(System.in);
        
        for(int i=0;i<nameArray.length;i++)
        {
            System.out.printf("Please enter #%d user name: ",(i+1));
            String name= input.nextLine();
            System.out.printf("Please enter #%d user age: ",(i+1));
            int age=input.nextInt();
            input.nextLine();
            nameArray[i]= name;
            ageArray[i]= age;
        }
        for(int i=0;i<nameArray.length;i++){
            System.out.printf("#%d User name is %s%n",i+1,nameArray[i]);
            System.out.printf("#%d User age is %s%n",i+1,ageArray[i]);
        }
    }
    
}
