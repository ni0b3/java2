/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Array;

import java.util.*;

/**
 *
 * @author ipd13
 */
public class RandomWeather {

    public static void main(String[] args) {
        //Scanner input = new Scanner(System.in);
        //System.out.println("Press any Key to generate a random Temperature");
        //String key = input.nextLine();

whatsTheTemp(weatherGenerator());
    }

    // generates number from -30 to 30
    public static double weatherGenerator() {
        double temp = Math.random() * 61 - 30;
        System.out.printf("The Temp is %.0f%n", temp);
        return temp;
    }

    public static int whatsTheTemp(double temp) {
        if (temp < -15) {
            System.out.printf("The tempperature is %.1f and it is VERY VERY FRIGID%n", temp);
        } else if (temp < 0) {
            System.out.printf("The tempperature is %.1f and it is FREEZING%n", temp);
        } else if (temp <= 15) {
            System.out.printf("The tempperature is %.1f and it is Spring or Fall%n", temp);
        } else {
            System.out.printf("The tempperature is %.1f and This is what I like%n", temp);
        }
        return 1;
    }
}
