/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Array;

import java.util.*;

/**
 *
 * @author ipd13
 */
public class PrintCalendar {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter full year ");
        int year = input.nextInt();

        System.out.print("Enter month as an umber between 1 and 12");
        int month = input.nextInt();
        printMonth(year, month);
    }

    public static void printMonth(int year, int month) {
        System.out.print(month + " " + year);
        printMonthTitle(year, month);
        printMonthBody(year, month);
    }

    public static void printMonthTitle(int year, int month) {
        System.out.println("       " + getMonthName(month) + "   " + year);
        System.out.println("---------------------------------------");
        System.out.println(" Sun Mon Tue Wed Thu Fri Sat");
    }

    public static void printMonthBody(int year, int month) {
        int startDay = getStartDay(year, month);

        int numberOfDaysInMonth = getNumberOfDaysInMonth(year, month);
        int i = 0;
        for (i = 0; i < startDay; i++) {
            System.out.print("   ");
        }
        for (i = 0; i <= numberOfDaysinMonth; i++) {
            System.out.printf("%4d", i);
            if ((i + startDay) % 7 == 0) {
                System.out.println();
            }
            System.out.println();
        }
    }

    public static String getMonthName(int month) {
        String monthName = "";// decalre and initialize the variable so that it can return a value
        switch (month) {
            case 1:
                monthName = "January";
                break;
            case 2:
                monthName = "February";
                break;
            case 3:
                monthName = "March";
                break;
            case 4:
                monthName = "April";
                break;
            case 5:
                monthName = "May";
                break;
            case 6:
                monthName = "June";
                break;
            case 7:
                monthName = "July";
                break;
            case 8:
                monthName = "August";
                break;
            case 9:
                monthName = "September";
                break;
            case 10:
                monthName = "October";
                break;
            case 11:
                monthName = "November";
                break;
            case 12:
                monthName = "December";
                break;

        }
        return monthName;
    }

    public static int getStartDay(int year, int month) {
        final int START_DAY_FOR_JAN_1_1800 = 3;
        int totalNumberOfDays = getTotalNumberOfDays(year, month);
        return (totalNumberOfDays + START_DAY_FOR_JAN_1_1800) % 7;
    }

    public static int getTotalNumberOfDays(int year, int month) {
        return 10000;
    }

    public static int getNumberOfDaysInMonth(int year, int month) {
        return 31;
    }

    public static Boolean isLeapYear(int year) {
        return year % 4000 == 0 || (year % 4 == 0 && year % 100 != 0);
    }

}
