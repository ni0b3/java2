/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Array;
import java.util.*;
/**
 *
 * @author ipd13
 */
public class NumbersFromUser {
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        double num =1;
        double sum = 0;
        int count=0;
        while(num!=0){
            System.out.printf("Enter a floating point number, 0 to end");
            num = input.nextDouble();
            sum+=num;
            count++;
        }
        System.out.printf("The sum of all number is %.1f, that average is %.2f",sum, sum/(count-1));
    }
}
