/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10peoplefrom.file;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

class ParameterInvalidException extends Exception {
    public ParameterInvalidException(String message) {
        super(message);
    }
}

class Person {

    public Person(String name, int age) throws ParameterInvalidException {
        setName(name);
        setAge(age);
    }

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) throws ParameterInvalidException {
        if (name.length() < 2) {
            throw new ParameterInvalidException("Name must be at least 2 characters long");
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws ParameterInvalidException {
        if (age < 0 || age > 150) {
            throw new ParameterInvalidException("Age must be between 0 and 150");
        }
        this.age = age;
    }
}

public class Day10PeopleFromFile {

    static ArrayList<Person> people = new ArrayList<>();

    public static void main(String[] args) {
        try (Scanner fileInput = new Scanner(new File("people.txt"))) {
            while (fileInput.hasNextLine()) {
                String name = fileInput.nextLine();
                int age = fileInput.nextInt();
                fileInput.nextLine();
                Person p = new Person(name, age);
                people.add(p);
            }
        } catch (IOException ex) {
            System.out.println("File reading error: " + ex.getMessage());
        } catch (ParameterInvalidException ex) {
            System.out.println("Invalid data: " + ex.getMessage());
        }
        //
        for (Person p: people) {
            System.out.printf("Person: %s is %d y/o\n", p.getName(), p.getAge());
        }
    }

}
