/*
 DAY 10 HOMEWORK

Create a project Day10AirportTravel.

In the main directory of the project, using Notepad create file "airports.txt" which will contain lines such as:

YUL;Montreal;45.4697842;-73.7554174
YYZ;Toronto;43.6777215;-79.6270084
JFK;New York JFK;40.6413151;-73.7803278,17

(Add at least 3 more lines)

Create a class Airport initially like this:

class Airport {
	String code, city;
	double latitude, longitude;
}

Add a suitable constructor, getters and setters that ensure that:
- code is always 3 upper-case letters,
- city is non-null, not empty string,
- latitude and longitude are within the correct ranges.


Setters should throw the following exception:

class ParameterInvalidException extends Exception {
    public ParameterInvalidException(String message) {
        super(message);
    }
}

When your program starts you will read file "airports.txt" instantiate object type Airport for each of them and add them to

static ArrayList<Airport> airportList = new ArrayList<>();

Suggestion: read each line with nextLine(), then String.split(";") the result, parse doubles and pass to Airport() constructor.

Display the following menu to the user:

1. Show all airports (codes and city names)
2. Find distance between two airports by codes.
3. Find the 1 airport nearest to an airport given and display the distance.
4. Add a new airport to the list.
0. Exit.

On Exit your program must write back an updated list of airports to "airports.txt" file.

Suggestion: make a copy of "airports.txt" file just in case you lost data when overriding.


Implement the functionality listed in the menu.
 */
package day10airporttravel;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nopassword
 */
class ParameterInvalidException extends Exception {

    public ParameterInvalidException(String message) {
        super(message);
    }
}

class Airport {

    private String code;
    private String city;
    private double latitude;
    private double longitude;

    public Airport(String code, String city, double latitude, double longitude) throws ParameterInvalidException {
        setCode(code);
        setCity(city);
        setLatitude(latitude);
        setLongitude(longitude);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) throws ParameterInvalidException {
        if (!code.matches("[A-Z]{3}")) {
            throw new ParameterInvalidException("Airport code invalid");
        }
        this.code = code;

    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) throws ParameterInvalidException {
        // if (!((city != null) && (city.length() > 0))) {
        if ((city == null) || (city.length() == 0)) {
            throw new ParameterInvalidException("City must not be null or empty");
        }
        this.city = city;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) throws ParameterInvalidException {
        if (latitude > 90 || latitude < -90) {
            throw new ParameterInvalidException("Latitude invalid");
        }
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) throws ParameterInvalidException {
        if (longitude > 180 || longitude < -180) {
            throw new ParameterInvalidException("Longitude invalid");
        }
        this.longitude = longitude;
    }

}

public class Day10AirportTravel {

    static ArrayList<Airport> airportList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);

    private static void printAirports() {
        for (Airport airports : airportList) {
            System.out.printf("%s; %s; %f ; %.2f %n", airports.getCode(), airports.getCity(), airports.getLatitude(), airports.getLongitude());
        }
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static void getDistanceByCode() {
        System.out.println("Please enter the airport #1 code:");
        String askCode1 = input.nextLine().toUpperCase();
        System.out.println("Please enter the airport #2 code:");
        String askCode2 = input.nextLine().toUpperCase();
        Airport[] searchList = new Airport[2];
        for (int j = 0; j < airportList.size(); j++) {
            if (airportList.get(j).getCode().contains(askCode1)) {
                searchList[0] = airportList.get(j);
            }
        }
        for (int i = 0; i < airportList.size(); i++) {
            if (airportList.get(i).getCode().contains(askCode2)) {
                searchList[1] = airportList.get(i);
            }
        }
        System.out.printf("The distance is %.2f Kilometers\n", distance(searchList[0].getLatitude(), searchList[1].getLatitude(), searchList[0].getLongitude(), searchList[1].getLongitude()) / 1000);

    }

    private static double distance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);

        return dist;

    }

    private static void findNearestAirport() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void readData() {
        try (Scanner dataInput = new Scanner(new File("airports.txt"))) {
            while (dataInput.hasNextLine()) {
                try {
                    String line = dataInput.nextLine();
                    String data[] = line.split(";");
                    if (data.length != 4) {
                        System.out.println("Invalid data in line, skipping: " + line);
                        continue;
                    }
                    String code = data[0];
                    String city = data[1];
                    double lat = Double.parseDouble(data[2]);
                    double lng = Double.parseDouble(data[3]);
                    airportList.add(new Airport(code, city, lat, lng));
                } catch (NumberFormatException ex) {
                    System.out.println("Invalid data, unparsable number");
                } catch (ParameterInvalidException ex) {
                    System.out.println("Invalid data: " + ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println("Fatal error reading from file");
        }
    }

    public static int airportMenu() throws ParameterInvalidException {
        readData();
        System.out.println("Please Make a Selection: ");
        System.out.println("**************************");
        System.out.println();
        System.out.printf("1. Show all airports (codes and city names)%n2. Find distance between two airports by codes.%n3. Find the 1 airport nearest to an airport given and display the distance.%n4. Add a new airport to the list.%n0. Exit.%n");
        System.out.println();
        System.out.println("**************************");
        System.out.print("Your Selection: ");
        int choice = input.nextInt();
        input.nextLine();// clear input data

        switch (choice) {
            case 1:
                printAirports();
                break;
            case 2:
                getDistanceByCode();
                break;
            case 3:
                findNearestAirport();
                break;
            case 4:
                addAirport();
                break;
            case 0:
                System.out.println("Exiting. Good bye!");
                saveData();
                System.exit(0);
                break;
            default:
                if (choice < 0 || choice > 4) {
                    System.out.println("Invalid choice, try again.");
                }
        }
        return choice;
    }

    public static void addAirport() throws ParameterInvalidException {
        try (Scanner input = new Scanner(new File("airports.txt"))) {
            while (input.hasNextLine()) {
                String code = input.nextLine();
                String city = input.nextLine();
                double latitude = input.nextDouble();
                double longitude = input.nextDouble();
                input.nextLine();
                Airport newAirport = new Airport(code, city, latitude, longitude);
                airportList.add(newAirport);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Day10AirportTravel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) throws ParameterInvalidException {
        while (true) {
            airportMenu();
        }
    }

    private static void saveData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
