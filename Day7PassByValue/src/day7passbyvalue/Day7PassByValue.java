/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day7passbyvalue;

/**
 *
 * @author ipd13
 */

class Box{
    int value;
}
public class Day7PassByValue {

    static void doit(int a, double b, double[]data, Box box) {
        System.out.printf("2: a=%d b=%.2f, data[0]=%.2f, data[1]=%.2f, Box.value =%d\n", a, b,data[0],data[1],box.value);
        a++;
        b += a;
        box.value=777;
        data[0]=111;
        data[1]=222;
        System.out.printf("3: a=%d b=%.2f, data[0]=%.2f, data[1]=%.2f, Box.value =%d\n", a, b,data[0],data[1], box.value);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int v1 = 5;
        double d2 = 5.5;
        double[] array={v1,d2};
        Box bbb=new Box();
        bbb.value=987;
        System.out.printf("1: v1=%d d2=%.2f,  data[0]=%.2f, data[1]=%.2f, bbb.value =%d\n", v1, d2,array[0],array[1], bbb.value);
        doit(v1,d2, array,bbb);
        System.out.printf("4: v1=%d d2=%.2f,  data[0]=%.2f, data[1]=%.2f, bbb.value =%d\n", v1, d2,array[0],array[1], bbb.value);
    }

}
