/*

Quiz 1

>>> Create project Quiz1Task1

--Your program will declare an array of 10 integers.
Generate 10 random integer numbers from -25 to +25 range, both inclusive and place them in the array.

In another loop find and print out, one per line integers that are divisible without remainder both by 3 and 2 but not by 12.

In another loop find the sum and average of all numbers and print both out.

In another loop find the smallest number and print it out.

In another loop find the 2nd smallest number and print it out.

 */
package quiz1;

import java.util.Random;

/**
 *
 * @author ipd13
 */
public class Quiz1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

       
        int[] int10 = new int[10]; //Declare array of 10 integer
        int sum = 0;
        for (int i = 0; i < int10.length; i++) {
            int10[i] = (int) (Math.random() * -25 + 26);
            System.out.printf("int10[] is: %d%n",int10[i]);
        }

        System.out.printf("***********************In another loop find the sum and average of all numbers and print both out.*****************************%n");

        // In another loop find the sum and average of all numbers and print both out.
        for (int i = 0; i < int10.length; i++) {
            int10[i] = (int) (Math.random() * -25 + 26);
            sum += int10[i];
            System.out.println(int10[i]);
        }
        System.out.printf("The sum of the number is %d", sum);

        System.out.printf("***********************In another loop find the smallest number and print it out.*****************************%n");

        //In another loop find the smallest number and print it out.
        int smallest = Integer.MAX_VALUE;
int secondSmallest = 0;
        for (int i = 0; i < int10.length; i++) {
            int10[i] = (int) (Math.random() * -24 + 25);
            System.out.println(int10[i]);
             for (int j = 0; j < int10.length; j++)
            if (smallest > int10[i]) {
                smallest = int10[i];
                 if (int10[j] < secondSmallest)
                {
                    secondSmallest = int10[j];
                }
            }  
        }
        System.out.printf("The  2nd smallest number is: %d", secondSmallest);
        
        
        System.out.printf("*******************In another loop find the 2nd smallest number and print it out.*********************************%n");
        //In another loop find the 2nd smallest number and print it out.
        
    }

}
