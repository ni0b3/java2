/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package todomenu;

import java.util.*;

/**
 *
 * @author ipd13
 *
 *
 */

class ToDo {

    ArrayList<ToDo> toDoList = new ArrayList<>();
    //////Variables
    private String task;
    private String dueDate;
    private int hoursOfWork;

    //////Getters
    public String getTask() {
        return task;
    }

    public String getDueDate() {
        return dueDate;
    }

    public int getHours() {
        return hoursOfWork;
    }

    //////Setters
    public void setTask(String task) throws Exception {
        if (task.length() < 2 || task.length() > 50) {
            throw new Exception("Name must be at least 2 to 50 characters long");
        }
        this.task = task;
    }

    public void setDueDate(String dueDate) throws Exception {
        if (dueDate.length() < 2 || dueDate.length() > 20) {
            throw new Exception("Name must be at least 2 -20 characters long");
        }
        this.dueDate = dueDate;
    }

    public void setHours(int hoursOfWork) throws Exception {
        if (hoursOfWork <= 0) {
            throw new Exception("You didnt work, there is nothing to calculate");
        }
        this.hoursOfWork = hoursOfWork;
    }

    public void print() {
        System.out.printf("Todo: %s, %s  will take %d hour(s) of work.", task, dueDate, hoursOfWork);
    }

    //// contructors
    public ToDo(String task, String dueDate, int hoursOfWork) throws Exception {
        setTask(task);
        setDueDate(dueDate);
        setHours(hoursOfWork);
    }

}

public class ToDoMenu {

    static Scanner input = new Scanner(System.in);
static ArrayList<ToDo> toDoList = new ArrayList<>();
    /// method
    public static void option1() {
        System.out.println("Enter task description: ");
        String task = input.nextLine();
        System.out.println("Enter due Date: ");
        String dueDate = input.nextLine();
        System.out.println("Enter hours of work (integer): ");
        int hours = input.nextInt();
        input.nextLine();
        try {
            ToDo newInput = new ToDo(task, dueDate, hours);
            toDoList.add(newInput);
        } catch (Exception ex) {
            System.out.println("Error with making the ToDO List Brah: " + ex.getMessage());
        }
    }

    public static void option2() {
         System.out.println(toDoList);
        }
        
    

    public static void option3() {

    }

    public static void option4() {

    }

    public static void toDoMenu(int choice) {
        switch (choice) {
            case 1:
                option1();
                break;
            case 2:
                option2();
                break;
            case 3:
                option3();
                break;
            case 4:
                option4();
                break;
            case 0:
                choice = 0;
                break;
            default:
                System.out.println("This is not a valid option");
                System.exit(1);
               
        }
    }
    
    public static int printMenu(){
        System.out.printf("Please make a choice [0-4]:%n1. Add a todo%n2. List all todos (numbered)%n3. Delete a todo%n4. Modify a todo%n0. Exit%n");
        int choice =input.nextInt();
        input.nextLine();
        return choice;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       
       while (0 != printMenu()) {
            printMenu();
            toDoMenu(printMenu());
        } 
    }

}
