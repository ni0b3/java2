/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day08peopleverified;

/**
 *
 * @author ipd13
 */

class Person{
    private String name;
    private int age;

    public Person(String name, int age) throws Exception {
        setName(name);
        setAge(age);
    }
 public int getAge() {
        return age;
    }
 
  public String getName() {
        return name;
    }
 
    public void setName(String name) throws Exception {
        if (name.length() < 2) {
            throw new Exception("Name must be at least 2 characters long");
        }
        this.name = name;
    }

    public void setAge(int age) throws Exception {
        if (age < 0 || age > 150) {
            throw new Exception("Age must be between 0 and 150");
        }
        this.age = age;
    }

}
public class Day08PeopleVerified {

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Person p = new Person("Johnny", -33);
            p.setName("Momma");
            p.setAge(-1);

        } catch (Exception ex) {
            System.out.println("Error occured(2): " + ex.getMessage());
        }
    }

}
