/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day08todo;

import java.util.*;

/**
 *
 * @author nopassword
 */
final class ToDo {

    ///Variables
    private String task;
    private String dueDate;
    private int hoursOfWork;

    ///// Getters
    public String getTask() {
        return task;
    }

    public String getDueDate() {
        return dueDate;
    }

    public int getHoursOfWork() {
        return hoursOfWork;
    }

    ///// Setters
    public void setDueDate(String dueDate) throws Exception {
        if (dueDate.length() < 2 || dueDate.length() > 20) {
            throw new Exception("Due date must be 2-20 characters long");
        }
        this.dueDate = dueDate;
    }

    public void setHoursOfWork(int hoursOfWork) throws Exception {
        if (hoursOfWork < 0) {
            throw new Exception("Hours of work must be a non-negative number");
        }
        this.hoursOfWork = hoursOfWork;
    }

    public void setTask(String task) throws Exception {
        if (task.length() < 2 || task.length() > 50) {
            throw new Exception("Task description must be 2-50 characters long");
        }
        this.task = task;
    }

    // Function
    public void print() {
        System.out.printf("Todo: %s, %s, will take %d hour(s) of work.\n",
                task, dueDate, hoursOfWork);
    }

    public ToDo(String task, String dueDate, int hoursOfWork) throws Exception {
        setTask(task);
        setHoursOfWork(hoursOfWork);
        setDueDate(dueDate);
    }
}

public class Day08ToDo {

    static ArrayList<ToDo> toDoList = new ArrayList<>();  // set an array for teh todo list
    static Scanner input = new Scanner(System.in);

    public static int toDoMenu() {
        int choice;
        System.out.println();
        System.out.println("1. Add a Todo");
        System.out.println("2. List all Todos (numbered)");
        System.out.println("3. Delete a Todo");
        System.out.println("4. Modify a Todo");
        System.out.println("0. Exit");
        System.out.print("Your pick is: ");
        choice = input.nextInt();
        input.nextLine();

        switch (choice) {
            case 1:
                addToDo();
                break;
            case 2:
                displayList();
                break;
            case 3:
                deleteList();
                break;
            case 4:
                modifyList();
                break;
            case 0:
                System.out.println("Exiting. Good bye!");
                System.exit(1);
            default:
                if (choice < 0 || choice > 4) {
                    System.out.println("Invalid choice, try again.");
                }
        }
        return choice;

    }

    private static void addToDo() {
        try {
            System.out.println("Adding a todo");
            System.out.print("Enter task description: ");
            String task = input.nextLine();
            System.out.print("Enter due Date: ");
            String dueDate = input.nextLine();
            System.out.print("Enter hours of work (integer): ");
            int hours = input.nextInt();
            input.nextLine(); // consume the left over newline character
            ToDo todo = new ToDo(task, dueDate, hours);
            toDoList.add(todo);
            System.out.println("You've created the following todo:");
            todo.print();
        } catch (Exception ex) {
            System.out.println("Error creating todo: " + ex.getMessage());
        }
    }

    private static void displayList() {
        for (int i = 0; i < toDoList.size(); i++) {//get the size of the array
            System.out.print(i + 1 + ". ");
            toDoList.get(i).print();
        }
    }

    private static void deleteList() {
        for (int i = 0; i < toDoList.size(); i++) {//get the size of the array
            System.out.print(i + 1 + ". ");
            toDoList.get(i).print();
        }
        System.out.println("Which todo you would like to delete?");
        int delEntry = input.nextInt(); // the entry to delete

        if (toDoList.size() > 0 && delEntry < toDoList.size()) {
            try {

                toDoList.remove(delEntry - 1);

                System.out.println("The entry " + delEntry + " is deleted");

            } catch (Exception ex) {
                System.out.println("Your List does not contain any data");
            }

        } else {
            System.out.println("No such entry!");

        }

    }

    private static void modifyList() {
        System.out.println("Which todo you would like to modify?");
        int modEntry = input.nextInt();
        System.out.println("Modyfying the following entry:");
        toDoList.get(modEntry - 1).print();
        input.nextLine();
        addToDo();
        try {
            toDoList.remove(modEntry - 1);
            ToDo newTask = new ToDo(toDoList.get(modEntry).getTask(),toDoList.get(modEntry).getDueDate(),toDoList.get(modEntry).getHoursOfWork());
            toDoList.add(modEntry - 1, newTask);
            System.out.println("You've modified the todo #" + modEntry + ":");
            newTask.print();
        } catch (Exception ex) {
            System.out.println("Error while modyfying Todo: " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        while (true) {
            toDoMenu();
        }
    }

}
