/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10sureinput;

import java.util.*;

public class Day10SureInput {
static Scanner input = new Scanner(System.in);

    public static int inputInt()
    {
        while(true) {
            try{
                int val=input.nextInt();
                //if(val == (int)val );  this line of code is not needed
                return val;
            } 
            catch(java.util.InputMismatchException ex){
       input.nextLine();// consume invalid input
            System.out.println("Input Error... sorry i only take digits");
        }
    }
    }
    
    public static void main(String[] args) {
      
            System.out.print("Enter a interger value:");
            int val = inputInt();
            System.out.println("You've entered: "+val);
       
    }
    
}
